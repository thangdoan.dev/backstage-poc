import { PluginDatabaseManager, UrlReader, errorHandler } from '@backstage/backend-common';
import { PluginTaskScheduler } from '@backstage/backend-tasks';
import { TemplateAction } from '@backstage/plugin-scaffolder-node';
import { CatalogApi } from '@backstage/catalog-client';
import {
  IdentityApi,
} from '@backstage/plugin-auth-node';
import {
  PermissionEvaluator
} from '@backstage/plugin-permission-common';
import { Config } from '@backstage/config';
import express from 'express';
import Router from 'express-promise-router';
import { Logger } from 'winston';

export interface RouterOptions {
  logger: Logger;
  config: Config;
  database: PluginDatabaseManager;
  reader: UrlReader;
  catalogClient: CatalogApi;
  scheduler?: PluginTaskScheduler;
  actions?: TemplateAction<any,any>[];
  identity?: IdentityApi;
  permissions?: PermissionEvaluator;
}

export async function createRouter(
  options: RouterOptions,
): Promise<express.Router> {
  const { logger } = options;

  const router = Router();
  router.use(express.json());

  router.get('/health', (_, response) => {
    logger.info('PONG!');
    response.json({ status: 'ok' });
  });
  router.use(errorHandler());
  return router;
}
