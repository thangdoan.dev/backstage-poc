import { createPlugin, createRoutableExtension } from '@backstage/core-plugin-api';

import { rootRouteRef } from './routes';

export const centralizedTemplatePlugin = createPlugin({
  id: 'centralized-template',
  routes: {
    root: rootRouteRef,
  },
});

export const CentralizedTemplatePage = centralizedTemplatePlugin.provide(
  createRoutableExtension({
    name: 'CentralizedTemplatePage',
    component: () =>
      import('./components/TemplateCentralizer').then(m => m.TemplateCentralizer),
    mountPoint: rootRouteRef,
  }),
);
