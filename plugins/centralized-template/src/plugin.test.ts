import { centralizedTemplatePlugin } from './plugin';

describe('centralized-template', () => {
  it('should export plugin', () => {
    expect(centralizedTemplatePlugin).toBeDefined();
  });
});
