import React, { useState } from 'react';
import { KeyboardArrowUp, KeyboardArrowDown, RemoveCircleOutline, Search } from '@material-ui/icons';
import { Collapse, IconButton, InputAdornment, Table, TableBody, TableCell, TableHead, TableRow, TextField } from '@material-ui/core';

export type Template = {
  variables: object[];
  name: string;
  link: string;
};

type ItemProps = {
  template: Template;
  index: number;
};


export const TemplateItem = ({ template, index }: ItemProps) => {

  const [open, setOpen] = useState(false);

  return (
    <>
      <TableRow>
        <TableCell>
          <IconButton
            aria-label="expand"
            size="small"
            onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
          </IconButton>
        </TableCell>
        <TableCell>
          {template.link ? template.link : <TextField InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Search />
              </InputAdornment>
            )
          }} />}
        </TableCell>
        <TableCell>
          {template.name}
        </TableCell>
        <TableCell>
          {index}
        </TableCell>
        <TableCell>
          <RemoveCircleOutline />
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell colSpan={5}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Variable</TableCell>
                  <TableCell>Type</TableCell>
                  <TableCell>Value</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {
                  template.variables.map((variable) => (
                    Object.entries(variable).map(([key, value]) => (
                      <TableRow>
                        <TableCell>
                          {key}
                        </TableCell>
                        <TableCell>
                          {value}
                        </TableCell>
                        <TableCell colSpan={3}>
                          <TextField type={value} />
                        </TableCell>
                      </TableRow>
                    ))
                  ))
                }
              </TableBody>
            </Table>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  )
};

