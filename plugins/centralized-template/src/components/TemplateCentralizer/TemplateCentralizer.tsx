import React from 'react';
import { Grid, Button } from '@material-ui/core';
import {
  Header,
  Page,
  Content,
  ContentHeader,
  HeaderLabel,
  SupportButton
} from '@backstage/core-components';
import { TemplateFetcher } from '../TemplateFetcher';

export const TemplateCentralizer = () => {
  return (
    <Page themeId="tool">
      <Header title="Template Centralizer" subtitle="Custom Template Generator At Ease">
        <HeaderLabel label="Owner" value="ThangDM" />
        <HeaderLabel label="Version" value="Alpha 0.1" />
      </Header>
      <Content>
        <ContentHeader title="Composer">
          <Button color='primary' >Add a Template</Button>
          <SupportButton>A description of your plugin goes here.</SupportButton>
        </ContentHeader>
        <Grid container spacing={3} direction="column">
          <Grid item>
            <TemplateFetcher />
          </Grid>
        </Grid>
      </Content>
    </Page>
  )
};

