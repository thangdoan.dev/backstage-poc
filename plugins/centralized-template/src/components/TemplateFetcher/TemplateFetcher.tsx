import React from 'react';
import { TemplateItem } from '../TemplateItem';
import { Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';

const templates = [
  {
    "variables": [
      {
        "name": "string",
        "instances": "number",
      }
    ],
    "name": "ECS",
    "link": "acbcd.com/template-test"
  }
]

export const TemplateFetcher = () => {
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell />
          <TableCell>Template</TableCell>
          <TableCell>Name</TableCell>
          <TableCell>Steps</TableCell>
          <TableCell></TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {
          templates.map((row, index) => (
            <TemplateItem template={row} index={index + 1} />
          ))
        }
      </TableBody>
    </Table>
  );
};
