import React from 'react';
import { createDevApp } from '@backstage/dev-utils';
import { centralizedTemplatePlugin, CentralizedTemplatePage } from '../src/plugin';

createDevApp()
  .registerPlugin(centralizedTemplatePlugin)
  .addPage({
    element: <CentralizedTemplatePage />,
    title: 'Root Page',
    path: '/centralized-template'
  })
  .render();
